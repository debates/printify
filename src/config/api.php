<?php

return [
    'country_code' => env('DEFAULT_COUNTRY_CODE', 'US'),
    'order_per_period' => env('ORDERS_PER_PERIOD', 2),
    'seconds_in_period' => env('SECONDS_IN_PERIOD', 30),
    'providers' => [
        \Torann\GeoIP\GeoIPServiceProvider::class,
    ],
    'aliases' => [
        'GeoIP' => \Torann\GeoIP\Facades\GeoIP::class,
    ]
];
