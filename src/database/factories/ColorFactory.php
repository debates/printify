<?php

use App\Color;
use Faker\Generator as Faker;

$factory->define(App\Color::class, function (Faker $faker) {
    return [
        'color' => $faker->randomElement(Color::TYPES)
    ];
});
