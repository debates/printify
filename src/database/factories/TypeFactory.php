<?php

use App\Type;
use Faker\Generator as Faker;

$factory->define(App\Type::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(Type::TYPES)
    ];
});
