<?php

use App\Color;
use App\Type;
use App\Size;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'price' => $faker->randomNumber(2),
        'color_id' => Color::orderByRaw('RAND()')->first()->id,
        'type_id' => Type::orderByRaw('RAND()')->first()->id,
        'size_id' => Size::orderByRaw('RAND()')->first()->id,
    ];
});
