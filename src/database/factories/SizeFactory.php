<?php

use App\Size;
use Faker\Generator as Faker;

$factory->define(App\Type::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(Size::TYPES)
    ];
});
