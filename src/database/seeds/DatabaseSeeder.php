<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->call(ColorsTableSeeder::class)
            ->call(TypesTableSeeder::class)
            ->call(SizesTableSeeder::class)
            ->call(ProductsTableSeeder::class)
        ;
    }
}
