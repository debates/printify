<?php

use App\Color;
use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Color::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach (Color::TYPES as $color) {
            Color::create([
                'color' => $color,
            ]);
        }
    }
}
