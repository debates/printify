<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    const TYPES = [
        'shirt',
        'hoodie',
        'long sleeve',
        'shoes',
        'sportswear',
        'sweatshirt',
        'tank pop',
        'bag',
        'jewelry',
        'phone case',
        'sock',
    ];

    protected $fillable = ['type'];
}
