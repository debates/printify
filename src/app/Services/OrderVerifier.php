<?php

namespace App\Services;

use App\Order;
use App\Product;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderVerifier
{

    const PRICE_MIN_LIMIT = 10.00;

    public static function validate(Order $order)
    {
        if($order->total_price < self::PRICE_MIN_LIMIT)
            throw new BadRequestHttpException('You need to order more than ' . self::PRICE_MIN_LIMIT . '$');

        $lastPeriod = Carbon::now()->subSeconds(config('api.seconds_in_period'));

        $ordersCount = Order
            ::where('country_code', '=', 'US')
            ->where('created_at', '>', $lastPeriod)
            ->where('created_at', '<', Carbon::now())
            ->get()->count();

        if($ordersCount) throw new BadRequestHttpException('Please, recreate order a bit later.');
    }
}