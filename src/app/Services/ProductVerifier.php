<?php

namespace App\Services;

use App\Product;
use Illuminate\Support\Collection;

class ProductVerifier
{
    public function validate(Product $product): Collection
    {
        return Product
            ::where('size_id', '=', $product->size_id)
            ->where('color_id', '=', $product->color_id)
            ->where('type_id', '=', $product->type_id)
            ->get()
        ;
    }
}