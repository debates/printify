<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

class OrderCalculator {

    public static function getTotalPrice(Collection $products): float
    {
        $total = 0.00;
        foreach ($products->toArray() as $product) {
            $total += $product['price'];
        }

        return $total;
    }
}