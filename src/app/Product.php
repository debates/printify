<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'color_id', 'type_id', 'size_id'];

    protected $casts = ['price' => 'float'];

    public function color()
    {
        return $this->hasOne(Color::class, 'color_id');
    }

    public function productType()
    {
        return $this->hasOne(Type::class, 'type_id');
    }

    public function size()
    {
        return $this->hasOne(Size::class, 'size_id');
    }
}
