<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    const TYPES = [
        'small',
        'medium',
        'large',
        'extra large'
    ];

    protected $fillable = ['type'];
}
