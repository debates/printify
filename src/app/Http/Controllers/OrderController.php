<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Services\OrderCalculator;
use App\Services\OrderVerifier;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Validator;

class OrderController extends Controller
{
    /**
     * Get list of order
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Order::with('products')->get());
    }

    /**
     * Get specific order
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $order = Order::with('products')->find($id);
        return $order ? response()->json($order) : response()->json(null, 404);
    }

    /**
     * Store new order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'products' => 'required|array',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors());
        }

        $products = Product::findMany($request->get('products'));
        if($products->count() !== count($request->get('products'))){
            throw new BadRequestHttpException('Please, check your products list.');
        }

        $order = new Order([
            'total_price' => OrderCalculator::getTotalPrice($products),
            'quantity' => $products->count(),
            'country_code' => geoip($request->ip())->getAttribute('iso_code') ?? config('api.country_code'),
        ]);

        OrderVerifier::validate($order);

        $order->save();

        $products->each(function ($product) use ($order) {
            $order->products()->attach($product);
        });

        $response = $order->toArray();
        $response['products'] = $products->toArray();

        return response()->json($response, 201);
    }

    public function update(Request $request, string $id): JsonResponse
    {
        $validation = Validator::make($request->all(), [
            'products' => 'required|array',
            'products.*' => 'integer',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors());
        }

        $order = Order::find($id);

        if (!$order) return response()->json(null, 404);

        $products = Product::findMany($request->get('products'));
        if($products->count() !== count($request->get('products'))){
            throw new BadRequestHttpException('Please check you products list.');
        }

        $order->products()->detach();
        $products->each(function ($product) use ($order) {
            $order->products()->attach($product);
        });

        $response = $order->toArray();
        $response['products'] = $products->toArray();

        return response()->json($response, 200);
    }

    public function delete(string $id): JsonResponse
    {
        $order = Order::find($id);
        if(!$order) return response()->json(null, 404);

        $order->delete();

        return response()->json($order, 200);
    }
}
