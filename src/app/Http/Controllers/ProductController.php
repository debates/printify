<?php

namespace App\Http\Controllers;

use App\Color;
use App\Product;
use App\Services\ProductVerifier;
use App\Type;
use App\Size;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Validator;

class ProductController extends Controller
{
    /**
     * Get list of products
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Product::all());
    }

    /**
     * Get specific product
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $product = Product::find($id);
        return $product ? response()->json($product) : response()->json(null, 404);
    }

    /**
     * Store new product
     * @param Request $request
     * @param ProductVerifier $productVerifier
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ProductVerifier $productVerifier): JsonResponse
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'size_id' => 'required',
            'color_id' => 'required',
            'type_id' => 'required',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors());
        }

        $size = Size::find((int) $request->get('size_id'));
        $color = Color::find((int) $request->get('color_id'));
        $type = Type::find((int) $request->get('type_id'));

        if(!$size || !$color || !$type) throw new BadRequestHttpException('Please, check your attributes list');

        $product = new Product([
            'name' => $request->get('name'),
            'price' => $request->get('price'),
            'size_id' => $size->id,
            'color_id' => $color->id,
            'type_id' => $type->id,
        ]);

        if($productVerifier->validate($product)->count())
            throw new BadRequestHttpException('Such attributes already exists.');

        $product->save();

        return $product ? response()->json($product, 201) : response()->json(null, 400);
    }

    public function update(Request $request, string $id): JsonResponse
    {
        $updateFields = [];

        if($request->get('name')) $updateFields['name'] = $request->get('name');
        if($request->get('price')) $updateFields['price'] = $request->get('price');
        if(Size::find((int) $request->get('size_id'))) $updateFields['size_id'] = $request->get('size_id');
        if(Color::find((int) $request->get('color_id'))) $updateFields['color_id'] = $request->get('color_id');
        if(Type::find((int) $request->get('type_id'))) $updateFields['type_id'] = $request->get('type_id');

        $product = Product::find($id);

        if (!$product) return response()->json(null, 404);

        $product->update($updateFields);

        return response()->json($product, 200);
    }

    public function delete(string $id): JsonResponse
    {
        $product = Product::find($id);
        if(!$product) return response()->json(null, 404);

        $product->delete();

        return response()->json($product, 200);
    }
}
