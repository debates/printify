<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    const TYPES = [
        'red',
        'orange',
        'yellow',
        'green',
        'blue',
        'purple',
        'pink',
        'brown',
        'gray',
        'black',
        'white',
    ];

    protected $fillable = ['color'];
}
