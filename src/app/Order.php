<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['total_price', 'quantity', 'country_code'];
    
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
