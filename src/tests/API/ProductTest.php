<?php

namespace Tests\API;

use App\Color;
use App\Size;
use App\Type;
use Faker\Factory;
use Faker\Generator;
use ProductsTableSeeder;
use Tests\TestCase;

class ProductTest extends TestCase
{

    /** @var Generator */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function test_get_list_products()
    {
        $response = $this->get('/api/products/');
        $decodedResponse = $response->decodeResponseJson();

        $response->assertJsonCount(ProductsTableSeeder::CREATE_PRODUCT_QUANTITY);
        $response->assertJsonStructure([
            'id',
            'name',
            'price',
            'color_id',
            'type_id',
            'size_id',
            'created_at',
            'updated_at'
        ], $decodedResponse[0]);
        $response->assertStatus(200);
    }

    public function test_post_product(): array
    {
        // To prevent code break, if such color && size && type already exists
        $color = new Color(['color' => 'black']);
        $color->save();

        $size = new Size(['type' => 'tiny']);
        $size->save();

        $type = new Type(['type' => 'jeans']);
        $type->save();

        $testData = [
            'name' => $this->faker->name(),
            'price' => $this->faker->randomNumber(2),
            'color_id' => $color->id,
            'type_id' => $type->id,
            'size_id' => $size->id,
        ];
        $response = $this->post('/api/products/', $testData);

        $decodedResponse = $response->decodeResponseJson();
        $response->assertJsonFragment($testData);
        $response->assertJsonStructure([
            'id',
            'name',
            'price',
            'color_id',
            'type_id',
            'size_id',
            'created_at',
            'updated_at'
        ], $decodedResponse);
        $response->assertStatus(201);

        return [
            'id' => $decodedResponse['id'],
            'color' => $color,
            'size' => $size,
            'type' => $type,
        ];
    }

    /**
     * @param array $productData
     * @depends test_post_product
     */
    public function test_put_product(array $productData)
    {
        $testData = [
            'name' => $this->faker->name(),
            'price' => $this->faker->randomNumber(2),
            'color_id' => $this->faker->numberBetween(1, count(Color::TYPES)),
            'type_id' => $this->faker->numberBetween(1, count(Type::TYPES)),
            'size_id' => $this->faker->numberBetween(1, count(Size::TYPES)),
        ];
        $response = $this->put("/api/products/$productData[id]", $testData);

        $decodedResponse = $response->decodeResponseJson();
        $response->assertJsonFragment($testData);
        $response->assertJsonStructure([
            'id',
            'name',
            'price',
            'color_id',
            'type_id',
            'size_id',
            'created_at',
            'updated_at'
        ], $decodedResponse);
        $response->assertStatus(200);
    }

    /**
     * @param array $productData
     * @depends test_post_product
     */
    public function test_delete_product(array $productData)
    {
        $productData['color']->delete();
        $productData['size']->delete();
        $productData['type']->delete();

        $response = $this->delete("/api/products/$productData[id]");

        $response->assertStatus(200);
    }
}
