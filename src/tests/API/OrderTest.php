<?php

namespace Tests\API;

use App\Product;
use App\Services\OrderCalculator;
use Faker\Factory;
use Faker\Generator;
use ProductsTableSeeder;
use Tests\TestCase;

class OrderTest extends TestCase
{

    /** @var Generator */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /** @expectedException */
    public function test_post_order_bad_request_expectation()
    {

        $productsCount = 12;
        $testData = [
            'products' => $this->faker->randomElements(
                range(1, ProductsTableSeeder::CREATE_PRODUCT_QUANTITY), $productsCount
            )
        ];

        $testData['products'][] = 88888;
        $response = $this->post('/api/orders/', $testData);
        $response->assertStatus(400);
    }

    public function test_post_order()
    {
        $productsCount = 12;
        $testData = [
            'products' => $this->faker->randomElements(
                range(1, ProductsTableSeeder::CREATE_PRODUCT_QUANTITY), $productsCount
            )
        ];

        $response = $this->post('/api/orders/', $testData);
        $decodedResponse = $response->decodeResponseJson();

        $response->assertJsonStructure([
            'id',
            'products',
            'total_price',
            'quantity',
            'country_code',
        ], $decodedResponse);
        static::assertCount($productsCount, $decodedResponse['products']);
        static::assertEquals(
            $decodedResponse['total_price'],
            OrderCalculator::getTotalPrice(Product::findMany($testData['products']))
        );
        $response->assertStatus(201);

        return $decodedResponse['id'];
    }

    /**
     * @param int $orderId
     * @depends test_post_order
     */
    public function test_put_order(int $orderId)
    {
        $newProductsCount = 14;
        $testData = [
            'products' => $this->faker->randomElements(
                range(1, ProductsTableSeeder::CREATE_PRODUCT_QUANTITY), $newProductsCount
            )
        ];

        $response = $this->put("/api/orders/$orderId", $testData);

        $decodedResponse = $response->decodeResponseJson();
        $response->assertJsonStructure([
            'id',
            'products',
            'total_price',
            'quantity',
            'country_code',
        ], $decodedResponse);
        static::assertEquals(count($decodedResponse['products']), $newProductsCount);
        $response->assertStatus(200);
    }

    public function test_get_list_orders()
    {
        $response = $this->get('/api/orders/');
        $decodedResponse = $response->decodeResponseJson();

        $response->assertJsonCount(1);
        $response->assertJsonStructure([
            'id',
            'products',
            'total_price',
            'quantity',
            'country_code',
        ], $decodedResponse[0]);
        $response->assertStatus(200);
    }

    /**
     * @param int $orderId
     * @depends test_post_order
     */
    public function test_get_specific_order($orderId)
    {
        $response = $this->get("/api/orders/$orderId");
        $decodedResponse = $response->decodeResponseJson();

        $response->assertJsonStructure([
            'id',
            'products',
            'total_price',
            'quantity',
            'country_code',
        ], $decodedResponse
        );
        $response->assertStatus(200);
    }

    /**
     * @param int $orderId
     * @depends test_post_order
     */
    public function test_delete_order(int $orderId)
    {
        $response = $this->delete("/api/orders/$orderId");
        $response->assertStatus(200);
    }
}
